package com.linked.scheduledtasks.bean.po;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

/**
 * @author :dbq
 * @date : 2022/11/24 11:03
 */
public class SwitchInfoPO {
    /**
     * 开关主表id
     */
    private String switchId;
    /**
     * 开关名称
     */

    private String switchName;
    /**
     * 是否开启
     */

    private Boolean ifSwitch;
    /**
     *
     */

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 开关备注
     */

    private String switchNotes;


    /**
     *开关是否有时间处理。0、无；1、定时开关；2、延时开关
     */

    private Integer ifTime;

    /**
     *开启时间
     */
    private String onTime;

    /**
     *关闭时间
     */
    private String offTime;

    /**
     *延时时间
     */

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime delayedTime;


    public String getSwitchId() {
        return switchId;
    }

    public void setSwitchId(String switchId) {
        this.switchId = switchId;
    }

    public String getSwitchName() {
        return switchName;
    }

    public void setSwitchName(String switchName) {
        this.switchName = switchName;
    }

    public Boolean getIfSwitch() {
        return ifSwitch;
    }

    public void setIfSwitch(Boolean ifSwitch) {
        this.ifSwitch = ifSwitch;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public String getSwitchNotes() {
        return switchNotes;
    }

    public void setSwitchNotes(String switchNotes) {
        this.switchNotes = switchNotes;
    }

    public Integer getIfTime() {
        return ifTime;
    }

    public void setIfTime(Integer ifTime) {
        this.ifTime = ifTime;
    }

    public String getOnTime() {
        return onTime;
    }

    public void setOnTime(String onTime) {
        this.onTime = onTime;
    }

    public String getOffTime() {
        return offTime;
    }

    public void setOffTime(String offTime) {
        this.offTime = offTime;
    }

    public LocalDateTime getDelayedTime() {
        return delayedTime;
    }

    public void setDelayedTime(LocalDateTime delayedTime) {
        this.delayedTime = delayedTime;
    }
}
