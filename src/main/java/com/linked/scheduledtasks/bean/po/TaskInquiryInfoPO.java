package com.linked.scheduledtasks.bean.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.linked.universal.bean.scheduledtasks.injuiry.LinkedInquiry;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author :dbq
 * @date : 2023/5/8 16:54
 * @description : desc
 */
@Data
@TableName("task_inquiry_info")
public class TaskInquiryInfoPO {


    /**
     * 轮询表主键
     */
    @TableId(value = "inquiry_id",type = IdType.ASSIGN_UUID)
    private String inquiryId;

    /**
     * 轮询业务key
     */
    @TableField("inquiry_key")
    private String inquiryKey;

    /**
     * 轮询的地址
     */
    @TableField("url")
    private String url;

    /**
     * 参数
     */
    @TableField("json_param")
    private String jsonParam;

    /**
     * 供应商简称
     */
    @TableField("supplier_name")
    private String supplierName;

    /**
     * 供应商ip
     */
    @TableField("supplier_ip")
    private String supplierIp;

    /**
     * 执行次数
     */
    @TableField("do_count")
    private Integer doCount;

    @TableField("trace_id")
    private String traceId;

    /**
     * create_time
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * data_status
     */
    @TableField("data_status")
    private Integer dataStatus;

    public TaskInquiryInfoPO(LinkedInquiry param) {
        this.inquiryKey = param.getInquiryKey();
        this.url = param.getUrl();
        this.jsonParam = param.getJsonParam();
        this.supplierName = param.getSupplierName();
        this.supplierIp = param.getSupplierIp();
        this.doCount = param.getDoCount();
        this.traceId = param.getTraceId();
    }

    public TaskInquiryInfoPO() {
    }
}
