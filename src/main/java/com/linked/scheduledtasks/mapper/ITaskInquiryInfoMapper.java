package com.linked.scheduledtasks.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.linked.scheduledtasks.bean.po.TaskInquiryInfoPO;
import com.linked.universal.bean.scheduledtasks.injuiry.LinkedInquiry;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author :dbq
 * @date : 2023/5/8 16:53
 * @description : desc
 */
@Mapper
public interface ITaskInquiryInfoMapper extends BaseMapper<TaskInquiryInfoPO> {
    List<LinkedInquiry> queryOnInquiryList();

    List<LinkedInquiry> queryOnInquiryListByKey(@Param("key") String key);
}
