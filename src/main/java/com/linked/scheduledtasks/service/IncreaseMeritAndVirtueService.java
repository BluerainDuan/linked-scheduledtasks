package com.linked.scheduledtasks.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.linked.scheduledtasks.feign.UserManageFeign;
import com.linked.universal.bean.usermanage.info.LinkedUserInfo;
import com.linked.universal.common.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Random;

/**
 * @author :dbq
 * @date : 2022/11/22 15:06
 */
@Service
public class IncreaseMeritAndVirtueService {

    private final UserManageFeign userManageFeign;

    private final ObjectMapper mapper;

    @Autowired
    public IncreaseMeritAndVirtueService(UserManageFeign userManageFeign,ObjectMapper mapper) {
        this.mapper = mapper;
        this.userManageFeign = userManageFeign;
    }

    private final static Logger logger = LoggerFactory.getLogger(IncreaseMeritAndVirtueService.class);

    public void increaseMeritAndVirtue() {
        Result ret = userManageFeign.queryAllUserList();

        List<LinkedUserInfo> userList = null;
        try {
            String jsonStr = (String) ret.get("data");
            userList = mapper.readValue(jsonStr, new TypeReference<List<LinkedUserInfo>>() {
            });
//            userList = JSON.parseArray(jsonStr, LinkedUserInfo.class);
            Random rand = new Random();
            LinkedUserInfo luckyUser = userList.get(rand.nextInt(userList.size()));
            logger.info(luckyUser.getUserName() + "施主  阿弥陀佛！");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
