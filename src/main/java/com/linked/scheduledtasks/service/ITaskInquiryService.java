package com.linked.scheduledtasks.service;

import com.linked.scheduledtasks.bean.po.TaskInquiryInfoPO;
import com.linked.universal.bean.scheduledtasks.injuiry.LinkedInquiry;

import java.util.List;

/**
 * @author :dbq
 * @date : 2023/5/8 16:50
 * @description : desc
 */
public interface ITaskInquiryService {
    boolean addInquiry(TaskInquiryInfoPO param) throws Exception;

    boolean removeInquiry(String inquiryId) throws Exception;

    List<LinkedInquiry> queryOnInquiryList();

    boolean removeInquiryByTraceId(String inquiryKey,String traceId);
}
