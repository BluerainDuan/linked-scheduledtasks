package com.linked.scheduledtasks.service;

/**
 * @author :dbq
 * @date : 2023/5/12 9:50
 * @description : desc
 */
public interface ISendPaymentResultService {
    void sendMessage() throws Exception;
}
