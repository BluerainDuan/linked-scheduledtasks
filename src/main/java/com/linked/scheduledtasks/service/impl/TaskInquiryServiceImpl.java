package com.linked.scheduledtasks.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.linked.scheduledtasks.bean.po.TaskInquiryInfoPO;
import com.linked.scheduledtasks.mapper.ITaskInquiryInfoMapper;
import com.linked.scheduledtasks.service.ITaskInquiryService;
import com.linked.universal.bean.scheduledtasks.injuiry.LinkedInquiry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author :dbq
 * @date : 2023/5/8 16:51
 * @description : desc
 */
@Service
@Slf4j
public class TaskInquiryServiceImpl implements ITaskInquiryService {

    private final ITaskInquiryInfoMapper taskInquiryInfoMapper;

    private final ObjectMapper mapper;

    public TaskInquiryServiceImpl(ITaskInquiryInfoMapper taskInquiryInfoMapper, ObjectMapper mapper) {
        this.taskInquiryInfoMapper = taskInquiryInfoMapper;
        this.mapper = mapper;
    }

    @Override
    public boolean addInquiry(TaskInquiryInfoPO param) throws Exception {
        return taskInquiryInfoMapper.insert(param) > 0;
    }

    @Override
    public boolean removeInquiry(String inquiryId) throws Exception {
        TaskInquiryInfoPO tmp = new TaskInquiryInfoPO();
        tmp.setDataStatus(0);
        tmp.setInquiryId(inquiryId);
        return taskInquiryInfoMapper.updateById(tmp) > 0;
    }

    @Override
    public List<LinkedInquiry> queryOnInquiryList() {
        return taskInquiryInfoMapper.queryOnInquiryList();
    }

    @Override
    public boolean removeInquiryByTraceId(String inquiryKey, String traceId) {
        UpdateWrapper<TaskInquiryInfoPO> wrapper = new UpdateWrapper<>();
        wrapper.eq("inquiry_key", inquiryKey);
        wrapper.eq("trace_id", traceId);
        TaskInquiryInfoPO tmp = new TaskInquiryInfoPO();
        tmp.setDataStatus(0);
        return taskInquiryInfoMapper.update(tmp, wrapper) > 0;
    }
}
