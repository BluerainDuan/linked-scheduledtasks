package com.linked.scheduledtasks.service.impl;

import com.linked.scheduledtasks.bean.po.TaskInquiryInfoPO;
import com.linked.scheduledtasks.config.RedisKeysConfig;
import com.linked.scheduledtasks.feign.IBaseManageFeign;
import com.linked.scheduledtasks.mapper.ITaskInquiryInfoMapper;
import com.linked.scheduledtasks.service.ISendPaymentResultService;
import com.linked.scheduledtasks.util.HandleRedisUtils;
import com.linked.scheduledtasks.util.OkHttpUtils;
import com.linked.universal.bean.basemanage.generallog.LinkedGeneralLog;
import com.linked.universal.bean.basemanage.generallog.LinkedProject;
import com.linked.universal.bean.scheduledtasks.injuiry.LinkedInquiry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author :dbq
 * @date : 2023/5/12 9:50
 * @description : desc
 */
@Service
@Slf4j
public class SendPaymentResultServiceImpl implements ISendPaymentResultService {

    private final ITaskInquiryInfoMapper taskInquiryInfoMapper;

    private final HandleRedisUtils<LinkedInquiry> redisUtils;

    private final OkHttpUtils okHttpUtils;

    private final IBaseManageFeign baseManageFeign;

    private final RedisTemplate redisTemplate;


    public SendPaymentResultServiceImpl(RedisTemplate redisTemplate, ITaskInquiryInfoMapper taskInquiryInfoMapper, HandleRedisUtils<LinkedInquiry> redisUtils, OkHttpUtils okHttpUtils, IBaseManageFeign baseManageFeign) {
        this.taskInquiryInfoMapper = taskInquiryInfoMapper;
        this.redisUtils = redisUtils;
        this.okHttpUtils = okHttpUtils;
        this.baseManageFeign = baseManageFeign;
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void sendMessage() throws Exception {
        List<LinkedInquiry> inquiryDList = taskInquiryInfoMapper.queryOnInquiryListByKey("sendpayresult");
        inquiryDList.stream().forEach(tmp -> {
            String response_Json = okHttpUtils.doPostJson(tmp.getSupplierIp() + tmp.getUrl(), tmp.getJsonParam());

            String methodStatus = "";
            if (StringUtils.isEmpty(response_Json)) {

            } else if (response_Json.contains("200")) {
                methodStatus = "200";
                if (redisTemplate.opsForHash().hasKey(RedisKeysConfig.KEY_INQUIRY_DOCOUNT, tmp.getInquiryId())) {
                    //如果执行次数超出，就停止执行轮询
                    if ((int) redisTemplate.opsForHash().get(RedisKeysConfig.KEY_INQUIRY_DOCOUNT, tmp.getInquiryId()) >= tmp.getDoCount()) {
                        TaskInquiryInfoPO task = new TaskInquiryInfoPO();
                        task.setInquiryId(tmp.getInquiryId());
                        task.setDataStatus(0);
                        int task_count = taskInquiryInfoMapper.updateById(task);
                        if (task_count > 0) {
                            //删除累加器
                            redisTemplate.opsForHash().delete(RedisKeysConfig.KEY_INQUIRY_DOCOUNT, tmp.getInquiryId());
                        }
                    } else {
                        //累加
                        redisTemplate.opsForHash().increment(RedisKeysConfig.KEY_INQUIRY_DOCOUNT, tmp.getInquiryId(), 1);
                    }
                } else {
                    //初始化累加器
                    redisTemplate.opsForHash().put(RedisKeysConfig.KEY_INQUIRY_DOCOUNT, tmp.getInquiryId(), 1);
                }
            } else if (response_Json.contains("500")) {
                methodStatus = "500";
            }

            try {

                baseManageFeign.insertListkedLog(
                        LinkedGeneralLog.Builder()
                                .withLogProject(LinkedProject.Linked_ScheduledTasks)
                                .withLogPosition("/SendPaymentResultServiceImpl/sendMessage")
                                .withPositionName("轮询-发送支付结果")
                                .withLogOrder(3)
                                .withMethodStatus(methodStatus)
                                .withMethodParam(tmp.getJsonParam())
                                .withMethodResponse(response_Json)
                                .withTraceId(tmp.getTraceId())
                                .withRemarks(tmp.getSupplierIp() + tmp.getUrl())
                );
            } catch (Exception e) {
            }
        });

    }
}
