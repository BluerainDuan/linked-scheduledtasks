package com.linked.scheduledtasks.feign;

import com.linked.scheduledtasks.feign.fallback.UserManageFeignFallBack;
import com.linked.universal.common.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author :dbq
 * @date : 2022/11/3 15:36
 */
@FeignClient(value = "Linked-UserManage", fallback = UserManageFeignFallBack.class)
public interface UserManageFeign {

    /**
     * 查询所有用户接口
     *
     * */
    @PostMapping(value = "/usermanage/userhandle/queryAllUserList", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    Result queryAllUserList();

}
