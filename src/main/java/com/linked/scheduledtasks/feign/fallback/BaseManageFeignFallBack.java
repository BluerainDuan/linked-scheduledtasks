package com.linked.scheduledtasks.feign.fallback;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linked.scheduledtasks.bean.po.SwitchInfoPO;
import com.linked.scheduledtasks.feign.IBaseManageFeign;
import com.linked.universal.bean.basemanage.generallog.LinkedGeneralLog;
import com.linked.universal.common.Result;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author :dbq
 * @date : 2022/11/24 9:54
 */
@Slf4j
public class BaseManageFeignFallBack implements IBaseManageFeign {


    @Autowired
    private ObjectMapper mapper;


    @Override
    public Boolean turnSwitch(String switchName) throws Exception {
        if (log.isInfoEnabled()){
            log.info("转到开关接口，熔断");
        }
        return null;
    }

    @Override
    public Boolean updateSwitchInfo(SwitchInfoPO param) throws Exception {
        if (log.isInfoEnabled()){
            log.info("修改开关接口，熔断");
        }
        return null;
    }


    @Override
    public Result querySwitchList(SwitchInfoPO param) {
        if (log.isInfoEnabled()){
            log.info("查询开关列表接口，熔断");
        }
        return null;
    }

    @Override
    public void insertListkedLog(LinkedGeneralLog param) {
        if (log.isInfoEnabled()){
            log.info("添加Linked日志接口，熔断");
        }
    }
}
