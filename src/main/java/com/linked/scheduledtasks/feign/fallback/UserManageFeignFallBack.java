package com.linked.scheduledtasks.feign.fallback;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.linked.scheduledtasks.feign.UserManageFeign;
import com.linked.universal.common.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author :dbq
 * @date : 2022/11/3 15:37
 */
public class UserManageFeignFallBack implements UserManageFeign {

    private final static Logger LOGGER = LoggerFactory.getLogger(UserManageFeignFallBack.class);
    @Autowired
    private ObjectMapper mapper;

    @Override
    public Result queryAllUserList() {
        LOGGER.info("调用用户项目 查询所有用户接口 ----- 熔断！");
        return null;
    }
}
