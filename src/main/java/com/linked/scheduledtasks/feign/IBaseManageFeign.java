package com.linked.scheduledtasks.feign;

import com.linked.scheduledtasks.bean.po.SwitchInfoPO;
import com.linked.scheduledtasks.feign.fallback.BaseManageFeignFallBack;
import com.linked.universal.bean.basemanage.generallog.LinkedGeneralLog;
import com.linked.universal.common.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author :dbq
 * @date : 2022/11/24 9:53
 */
@FeignClient(value = "Linked-BaseManage", fallback = BaseManageFeignFallBack.class)
public interface IBaseManageFeign {
    /**
     * 转动开关
     */
    @PostMapping(value = "/basemanage/switchsetting/turnSwitch")
    Boolean turnSwitch(String switchName) throws Exception;

    /**
     * 转动开关
     */
    @GetMapping(value = "/basemanage/switchsetting/updateSwitchInfo")
    Boolean updateSwitchInfo(SwitchInfoPO param) throws Exception;

    @PostMapping(value = "/basemanage/switchsetting/querySwitchList")
    Result querySwitchList(SwitchInfoPO param);

    @PostMapping(value = "/basemanage/generallog/insertListkedLog")
    void insertListkedLog(LinkedGeneralLog param);
}
