package com.linked.scheduledtasks.schedule;

import com.linked.scheduledtasks.config.ScheduledTaskSwitchConfig;
import com.linked.scheduledtasks.service.ISendPaymentResultService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author :dbq
 * @date : 2023/5/12 9:44
 * @description : desc
 */
@Component
@Slf4j
public class PayTask {

    public final static long MINUTE = 60 * 1000;

    private final ISendPaymentResultService sendPaymentResultService;

    private final ScheduledTaskSwitchConfig scheduledTaskSwitchConfig;

    public PayTask(ISendPaymentResultService sendPaymentResultService, ScheduledTaskSwitchConfig scheduledTaskSwitchConfig) {
        this.sendPaymentResultService = sendPaymentResultService;
        this.scheduledTaskSwitchConfig = scheduledTaskSwitchConfig;
    }

    @Scheduled(fixedDelay = MINUTE * 1)
    @Async
    public void sendPaymentResult(){
        /**
         * 发送支付结果定时任务
         * */
        try {
            if (scheduledTaskSwitchConfig.isSendPaymentResultSwitch()) {
                sendPaymentResultService.sendMessage();
            }
        } catch (Exception e) {
            log.error("发送支付结果定时任务 异常", e);
        }
    }
}
