package com.linked.scheduledtasks.schedule;

import com.linked.scheduledtasks.config.ScheduledTaskSwitchConfig;
import com.linked.scheduledtasks.service.SwitchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author :dbq
 * @date : 2022/11/24 9:44
 */
@Component
public class BaseManageTask {
    public final static long MINUTE = 60 * 1000;

    private final static Logger logger = LoggerFactory.getLogger(BaseManageTask.class);

    private final SwitchService switchService;

    private final ScheduledTaskSwitchConfig scheduledTaskSwitchConfig;

    @Autowired
    public BaseManageTask(SwitchService switchService, ScheduledTaskSwitchConfig scheduledTaskSwitchConfig) {
        this.switchService = switchService;
        this.scheduledTaskSwitchConfig = scheduledTaskSwitchConfig;
    }

    /**
     * 延时开关定时任务
     */
    @Scheduled(fixedDelay = MINUTE * 1)
    @Async
    public void timeSwitch() throws Exception {
        /**
         * 定时开关
         * */
        try {
            if (scheduledTaskSwitchConfig.isAutoSwitch()) {
                switchService.autoSwitch();
            }

        } catch (Exception e) {
            logger.error("定时开关异常", e);
        }
        /**
         * 延时开关
         * */
        try {
            if (scheduledTaskSwitchConfig.isDelayedSwitch()) {
                switchService.delayedSwitch();
            }
        } catch (Exception e) {
            logger.error("延时开关异常", e);
        }


    }
}
