package com.linked.scheduledtasks.schedule;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linked.scheduledtasks.config.ScheduledTaskSwitchConfig;
import com.linked.scheduledtasks.feign.UserManageFeign;
import com.linked.scheduledtasks.service.IncreaseMeritAndVirtueService;
import com.linked.universal.common.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Random;

/**
 * @author :dbq
 * @date : 2022/11/3 15:15
 */
@Component
public class FirstAutoTask {

    public final static long MINUTE = 60 * 1000;

    private final IncreaseMeritAndVirtueService increaseMeritAndVirtueService;

    private final ScheduledTaskSwitchConfig scheduledTaskSwitchConfig;

    @Autowired
    public FirstAutoTask(IncreaseMeritAndVirtueService increaseMeritAndVirtueService, ScheduledTaskSwitchConfig scheduledTaskSwitchConfig) {
        this.increaseMeritAndVirtueService = increaseMeritAndVirtueService;
        this.scheduledTaskSwitchConfig = scheduledTaskSwitchConfig;
    }

    /**
     * 增加功德定时任务
     */
    @Scheduled(fixedDelay = MINUTE * 1)
    @Async
    public void increaseMeritAndVirtue() throws Exception {
        if (scheduledTaskSwitchConfig.isIncreaseMeritAndVirtueSwitch()) {
            increaseMeritAndVirtueService.increaseMeritAndVirtue();
        }

    }
}
