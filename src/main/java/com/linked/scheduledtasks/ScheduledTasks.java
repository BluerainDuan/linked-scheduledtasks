package com.linked.scheduledtasks;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author :dbq
 * @date : 2022/11/3 15:03
 */
@MapperScan("com.linked.scheduledtasks.mapper")
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@EnableScheduling
@EnableAsync
public class ScheduledTasks {
    public static void main(String[] args) {
        SpringApplication.run(ScheduledTasks.class, args);
    }
}
