package com.linked.scheduledtasks.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linked.scheduledtasks.bean.po.TaskInquiryInfoPO;
import com.linked.scheduledtasks.service.ITaskInquiryService;
import com.linked.universal.bean.scheduledtasks.injuiry.LinkedInquiry;
import com.linked.universal.common.LinkedPrompt;
import com.linked.universal.common.LinkedResult;
import com.linked.universal.common.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author :dbq
 * @date : 2023/5/8 16:49
 * @description : desc
 */
@RestController
@RequestMapping("taskinquiry")
@Slf4j
public class TaskInquiryController {


    private final ObjectMapper mapper;

    private final ITaskInquiryService taskInquiryService;


    public TaskInquiryController(ObjectMapper mapper, ITaskInquiryService taskInquiryService) {
        this.mapper = mapper;
        this.taskInquiryService = taskInquiryService;
    }

    @PostMapping("addInquiry")
    public LinkedResult addInquiry(@RequestBody LinkedInquiry param) throws Exception {
        if (log.isInfoEnabled()) {
            log.info("添加轮询接口 入参：{}", mapper.writeValueAsString(param));
        }
        boolean ret = false;
        TaskInquiryInfoPO tmp = new TaskInquiryInfoPO(param);
        try {
            ret = taskInquiryService.addInquiry(tmp);
        } catch (Exception e) {
            log.error("添加轮询接口 异常", e);
            return LinkedResult.Error(LinkedPrompt.ERROR_MESSAGE);
        }
        return ret ? LinkedResult.Success(tmp.getInquiryId()) : LinkedResult.OK(false, LinkedPrompt.CREATE_FAILURE);
    }

    @PostMapping("removeInquiry")
    public Result removeInquiry(String inquiryId) {
        if (log.isInfoEnabled()) {
            log.info("移除轮询接口 入参：" + inquiryId);
        }

        boolean ret = false;
        try {
            ret = taskInquiryService.removeInquiry(inquiryId);
        } catch (Exception e) {
            log.error("移除轮询接口 异常！", e);
            return Result.error("未知异常，请联系管理员！");
        }
        return ret ? Result.success() : Result.ok(false, "移除失败！");
    }

    @PostMapping("removeInquiryByTraceId")
    public LinkedResult removeInquiryByTraceId(String inquiryKey, String traceId) {
        if (log.isInfoEnabled()) {
            log.info("移除轮询接口 入参：" + traceId);
        }
        boolean ret = false;
        try {
            ret = taskInquiryService.removeInquiryByTraceId(inquiryKey, traceId);
        } catch (Exception e) {
            log.error("移除轮询接口 异常！", e);
            return LinkedResult.Error("未知异常，请联系管理员！");
        }
        return ret ? LinkedResult.Success() : LinkedResult.OK(false, "移除失败！");
    }

    @PostMapping("queryOnInquiryList")
    Result queryOnInquiryList() {

        List<LinkedInquiry> inquiryList = null;
        try {
            inquiryList = taskInquiryService.queryOnInquiryList();
        } catch (Exception e) {
            log.error("查询正在执行的轮询接口 异常", e);
            return Result.error("未知异常，请联系管理员！");
        }
        return !CollectionUtils.isEmpty(inquiryList) ? Result.success(inquiryList) : Result.ok(false, "未查询到数据！");
    }
}
