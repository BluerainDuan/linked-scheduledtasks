package com.linked.scheduledtasks.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author :dbq
 * @date : 2023/1/18 15:48
 * @description : desc
 */
@Data
@Configuration
@ConfigurationProperties("scheduledtaskswitch")
public class ScheduledTaskSwitchConfig {

    /**
     * 延时开关
     */
    private boolean delayedSwitch;

    /**
     * 定时开关
     */
    private boolean autoSwitch;

    /**
     * 增加功德开关
     */
    private boolean increaseMeritAndVirtueSwitch;

    /**
     * 发送支付结果开关
     * */
    private boolean sendPaymentResultSwitch;
}
