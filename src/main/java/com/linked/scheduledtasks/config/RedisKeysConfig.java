package com.linked.scheduledtasks.config;

/**
 * @author :dbq
 * @date : 2023/2/23 13:16
 * @description : desc
 */
public class RedisKeysConfig {

    //查询所有系统信息KEY
    public final static String KEY_INQUIRYLIST = "INQUIRYLIST";

    public final static String KEY_INQUIRY_DOCOUNT = "INQUIRYDOCOUNT";


}
